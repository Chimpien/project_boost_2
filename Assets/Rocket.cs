﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {

    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float mainThrust = 10f;
    [SerializeField] float lvlLoadDelay = 2f;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] AudioClip death;
    [SerializeField] AudioClip lvlUp;

    [SerializeField] ParticleSystem mainEngineParticles;
    [SerializeField] ParticleSystem deathParticles;
    [SerializeField] ParticleSystem lvlUpParticles;

    Rigidbody rigidBody;
    AudioSource audioSource;

    enum State { alive, dead, trans };
    State state = State.alive;
    private int level = 0;

    // Use this for initialization
    void Start ()
    {
        rigidBody = GetComponent<Rigidbody>();//get handle for the RB physics
        audioSource = GetComponent<AudioSource>();//get handle for the audiosource component

    }
	
	// Update is called once per frame
	void Update ()
    {
        if (state == State.alive)
        {
            RespondToThrust();
            RespondToRotate();
        }

        //Method for enabling and disabling debug mode depending on 'development build' tick box in build settings
        
        if(Debug.isDebugBuild)//always true in the editor, only changes depending on build settings when built and deployed for running outside the editor
        {
            RespondToDebugKeys();
        }
        
	}

    private void RespondToDebugKeys()
    {
        if (Input.GetKey(KeyCode.L))
        {
            ChangeLvl();
        }
    }

    private void RespondToThrust()
    {

        if (Input.GetKey(KeyCode.UpArrow))//get key stroke
        {
            ApplyThrust();
        }
        else
        {
            StopThrust();
        }
    }

    private void StopThrust()
    {
        if (audioSource.isPlaying)
        {
            audioSource.Stop();
        }
        mainEngineParticles.Stop();
    }

    private void ApplyThrust()
    {
        rigidBody.AddRelativeForce(Vector3.up * mainThrust);//apply a force using the physics engine
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(mainEngine);
        }
        mainEngineParticles.Play();
    }

    private void RespondToRotate()
    {                    
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            ApplyRotation(rcsThrust * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            ApplyRotation(-rcsThrust * Time.deltaTime);
        }                
    }

    private void ApplyRotation(float rotationThisFrame)
    {
        rigidBody.freezeRotation = true; //manual control of rotation only
        transform.Rotate(Vector3.forward * rotationThisFrame);
        rigidBody.freezeRotation = false;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (state != State.alive) return;

        switch(collision.gameObject.tag)
        {
            case "Friendly":
                print("OK");
                break;
            case "Finish":
                ChangeLvl();
                break;
            default:
                Die();
                break;
        }
    }

    private void ChangeLvl()
    {
        state = State.trans;
        int currentScnIdx = SceneManager.GetActiveScene().buildIndex;
        //print(currentScnIdx);
        level = currentScnIdx + 1;
        print(level);
        if(level == SceneManager.sceneCountInBuildSettings)
        {
            level = 0;
        }
        audioSource.Stop();        
        audioSource.PlayOneShot(lvlUp);
        lvlUpParticles.Play();
        Invoke("SceneChange", lvlLoadDelay);
    }

    private void Die()
    {
        state = State.dead;
        level=0;
        
        audioSource.Stop();        
        audioSource.PlayOneShot(death);
        
        //get handles for the renderers in the child objects that make up the rocket
        Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderers)
        {
            r.enabled = false;//disable all renderers, making the object invisible
        }

        deathParticles.GetComponent<Renderer>().enabled = true;//re-enable the death particle effect renderer
        
        deathParticles.Play();//play the death animation        
        Invoke("SceneChange", lvlLoadDelay);
    }

    private void SceneChange()
    {
        print("finish");
        SceneManager.LoadScene(level);        
        state = State.alive;
    }

    




}
