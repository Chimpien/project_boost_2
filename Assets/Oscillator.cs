﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class Oscillator : MonoBehaviour {

    [SerializeField] Vector3 movementVector = new Vector3(0,-10,0);
    [SerializeField] [Range(0, 1)] float movementFactor;
    [SerializeField] float period = 2f;

    Vector3 startPos;
	// Use this for initialization
	void Start ()
    {
        startPos = transform.position;//transform is the location and rotation of the parent object for the script
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (period <= Mathf.Epsilon) return;
        float cycles = Time.time / period;
        const float tau = 2 * Mathf.PI;
        movementFactor = (Mathf.Sin(cycles * tau)/2f) + 0.5f;
        Vector3 offset = movementVector * movementFactor;
        transform.position = startPos + offset;//the transform can be manipulated in script
		
	}
}
